(function(angular) {
	"use strict";

	angular
		.module('app')
		.controller('ListController', ListController);

	ListController.$inject = ['CarService', '$uibModal', '$scope', '$filter'];

	function ListController(CarService, $uibModal, $scope, $filter) {

		var vm = this;

		vm.cars = CarService.cars;
		vm.modal;
		vm.results = vm.cars;
		vm.search;

		vm.config = {
						itemsPerPage: 5,
						fillLastPage: false,
						maxPages: 5,
						paginatorLabels: {
							first: "‹‹‹",
							last: "›››",
							jumpAhead: "››",
							jumpBack: "‹‹",
							stepAhead: "›",
							stepBack: "‹",
						}
					}

		vm.delete = function(id) {
			CarService.delete(id);
		}

		vm.openModal = function(car) {

			console.log(car);
			vm.modal = $uibModal.open({
				animation: true,
				templateUrl: 'views/common/modal.html',
				backdrop: false,
				controller: function(){
					this.image = car.imagem;
					this.close = function() {
									vm.modal.close();
								}
				},
				controllerAs: 'ctrl'
			});
		}

		$scope.$watch(
			function(){
				return vm.search;
			},
			function(_new, old){
				vm.results = $filter('carFilter')(vm.cars, _new);
			}
		)

	}

})(window.angular);