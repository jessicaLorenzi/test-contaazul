(function(angular) {
	"use strict";

	angular
		.module('app')
		.controller('FormController',  FormController);

	FormController.$inject = ['CarService', '$state', '$stateParams'];

	function  FormController(CarService, $state, $stateParams) {

		var vm = this;

		vm.car = {};

		vm.isUpdate = $stateParams.id !== undefined;

		if ($stateParams.id) {
			vm.car = CarService.get($stateParams.id);
		}

		vm.save = function(car) {
			CarService.add(vm.car);
			$state.go('home');
		}

		vm.back = function() {
			$state.go('home');
		}

	}

})(window.angular);