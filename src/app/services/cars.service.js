(function(angular) {
	"use strict";

	angular
		.module('app')
		.factory('CarService', CarService);

	CarService.$inject = [];

	function CarService() {

		var svc = {}

		svc.cars = 	[
						{
							"id" : 1,
							"combustivel" : "Flex",
							"imagem" : null,
							"marca" : "Volkswagem",
							"modelo" : "Gol",
							"placa" : "FFF5498",
							"valor" : "20000"
						},
						{
							"id" : 2,
							"combustivel" : "Gasolina",
							"imagem" : null,
							"marca" : "Volkswagem",
							"modelo" : "Fox",
							"placa" : "FOX4125",
							"valor" : "20000"
						},
						{
							"id" : 3,
							"combustivel" : "Álcool",
							"imagem" : "http://carros.ig.com.br/fotos/2010/290_193/Fusca2_290_193.jpg",
							"marca" : "Volkswagen",
							"modelo" : "Fusca",
							"placa" : "PAI4121",
							"valor" : "20000"
						}
					]
		svc.car = {}

		svc.add = function(car) {

			if (!car.id) {
				car.id = generateId();
				svc.cars.push(car);
			}
		}

		svc.get = function(id) {

			if (!id) {
				return;
			}

			svc.cars.forEach(function(car) {

				if (car.id == id) {
					svc.car = car;
				}

			});

			return svc.car;
		}

		svc.delete = function(id) {

			svc.cars.forEach(function(car) {

				if(car.id == id) {
					var i = svc.cars.indexOf(car);
					svc.cars.splice(i,1);
				}

			});
		}

		function generateId() {
			return (new Date()).getTime();
		}

		return svc;
	}



})(window.angular);
