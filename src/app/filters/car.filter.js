(function(angular) {
	"use strict";

	angular
	.module("app")
	.filter('carFilter', [carFilter]);

	function carFilter () {
		return function(cars, string) {

			if (!string)
				return cars;

			var filtered = [];

			angular.forEach(cars, function(car) {

				if ( car.marca.toUpperCase().indexOf(string.toUpperCase()) > -1 ||
					car.combustivel.toUpperCase().indexOf(string.toUpperCase()) > -1) {

					filtered.push(car);
				}
			});

			return filtered;
		};
	}

})(window.angular);
