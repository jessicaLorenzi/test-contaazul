(function(angular) {
  "use strict";

  angular.module('app', [
  	'ui.router',
  	'ui.bootstrap',
  	'angular-table',
  	'ui.utils.masks',
  	'ui.mask'
  ])

})(window.angular);
