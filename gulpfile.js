const SOURCES = {
  vendorFonts: ['./node_modules/bootstrap/fonts/**'],
  vendorCss: [
    './node_modules/bootstrap/dist/css/bootstrap.min.css',
    './node_modules/angular-ui-bootstrap/dist/ui-bootstrap-csp.css'
  ],
  vendorJs: [
    './node_modules/angular/angular.js',
    './node_modules/angular-ui-router/release/angular-ui-router.js',
    './node_modules/angular-ui-bootstrap/dist/ui-bootstrap.js',
    './node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',
    './node_modules/angular-table/dist/angular-table.js',
    './node_modules/angular-input-masks/releases/angular-input-masks-standalone.min.js',
    './node_modules/angular-ui-mask/dist/mask.min.js'

  ],
  less: ['./src/assets/less/main.less'],
  fonts: ['./src/assets/fonts/**'],
  images: ['./src/assets/img/**'],
  scripts: ['./src/app/**/*.js'],
  index: [
    './src/index.html'
  ],
  views: ['./src/views/**']
}

const gulp = require('gulp'),
      concat = require('gulp-concat'),
      less = require('gulp-less'),
      minifyCss = require('gulp-minify-css'),
      rename = require('gulp-rename'),
      connect = require('gulp-connect');

gulp.task('default', ['build', 'watch', 'connect'])
gulp.task('build', ['assets', 'html', 'vendor'])
gulp.task('watch', () => gulp.watch(['./src/**'], ['build']))
gulp.task('vendor', ['vendorCss', 'vendorJs', 'vendorFonts'])
gulp.task('assets', ['less', 'fonts', 'images', 'scripts'])
gulp.task('html', ['index', 'views'])

gulp.task('connect', connectTask);

gulp.task('vendorCss', vendorCssTask)
gulp.task('vendorJs', vendorJsTask)
gulp.task('vendorFonts', vendorFontsTask)

gulp.task('less', lessTask)
gulp.task('fonts', fontsTask)
gulp.task('images', imagesTask)
gulp.task('scripts', scriptsTask)
gulp.task('index', indexTask)
gulp.task('views', viewsTask)


function connectTask() {
  connect.server({
    name: 'Default',
    root: ['dist'],
    port: 4000,
    fallback: 'dist/index.html'
  });
}

function vendorFontsTask(done) {
  gulp.src(SOURCES.vendorFonts)
    .pipe(gulp.dest('./dist/fonts/'))
    .on('end', done)
}

function vendorCssTask(done) {
  gulp.src(SOURCES.vendorCss)
    .pipe(concat('vendor.css'))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./dist/css/'))
    .on('end', done)
}

function vendorJsTask(done) {
  gulp.src(SOURCES.vendorJs)
    .pipe(concat('vendor.js'))
    .pipe(rename({ extname: '.min.js' }))
    .pipe(gulp.dest('./dist/js/'))
    .on('end', done)
}

function lessTask(done) {
  gulp.src(SOURCES.less)
    .pipe(less())
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./dist/assets/css/'))
    .on('end', done)
}

function fontsTask(done) {
  gulp.src(SOURCES.fonts)
    .pipe(gulp.dest('./dist/assets/fonts/'))
    .on('end', done)
}

function imagesTask(done) {
  gulp.src(SOURCES.images)
    .pipe(gulp.dest('./dist/assets/img/'))
    .on('end', done)
}

function scriptsTask(done) {
  gulp.src(SOURCES.scripts)
    .pipe(concat('main.js'))
    .pipe(rename({ extname: '.min.js' }))
    .pipe(gulp.dest('./dist/js/'))
    .on('end', done)
}

function indexTask(done) {
  gulp.src(SOURCES.index)
    .pipe(gulp.dest('./dist/'))
    .on('end', done)
}

function viewsTask(done) {
  gulp.src(SOURCES.views)
    .pipe(gulp.dest('./dist/views/'))
    .on('end', done)
}